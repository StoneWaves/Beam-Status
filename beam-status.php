<?php
 /*
 	* Plugin Name: Beam Status
	* Plugin URI: 
	* Description: A plugin to get the status of a beam stream
	* Version: 1.0
	* Author: Chris Allen
 	* Author URI: 
  */

 	include 'beam-status-settings.php';

 	// Plugin Code
 	function status(){
 		$beam_username = get_option('beam_username');
		$json = file_get_contents('http://mixer.com/api/v1/channels/' .$beam_username);
		$obj = json_decode($json, true);

		if($obj['online'] == true){
			echo '<div style="position:absolute; top: 60px; left:10px;" class="alert alert-info alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>'.$beam_username.'</strong> is live on <a href="http://beam.pro/'.$beam_username.'" class="alert-link">Beam.pro</a> with "';
			echo $obj['name'];
			echo '"</div>';
		}
 	}

	add_action( 'wp', 'status');
	add_shortcode( 'beam', 'status' );
?>
