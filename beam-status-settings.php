<?php
	// Setting page
	add_action('admin_menu', 'beam_status_plugin_setup_menu');
 
	function beam_status_plugin_setup_menu(){
	    add_menu_page( 'Beam Status Page', 'Beam Status', 'manage_options', 'beam-status', 'beam_status_settings_page' );
	}

	function display_beam_element()
	{
		?>
	    	<input type="text" name="beam_username" id="beam_username" value="<?php echo get_option('beam_username'); ?>" />
	    <?php
	}

	function display_theme_panel_fields()
	{
		add_settings_section("section", "All Settings", null, "beam-status-options");
		add_settings_field("beam_username", "beam.pro username", "display_beam_element", "beam-status-options", "section");
		register_setting("section", "beam_username");
	}

	add_action("admin_init", "display_theme_panel_fields");

	function beam_status_settings_page(){
		?>
	    <div class="wrap">
	    <h1>Beam Status Settings</h1>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields("section");
	            do_settings_sections("beam-status-options");      
	            submit_button(); 
	        ?>          
	    </form>
		</div>
		<?php
	}
?>